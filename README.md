# GitLab Trello Power-Up

This repository contains the code used for GitLab's [Trello Power-Up](https://developers.trello.com/power-ups/intro) which was released in the 9.4 milestone.

## Getting Started (Development)

1. `yarn install`
2. `yarn serve` and take note of the https ngrok url
3. Go to [Trello's Power-Ups Admin Console](https://trello.com/power-ups/admin)
4. Select a team to test
5. Fill out `Trello Joint Development Agreement`
5. Select `Create New Power-Up`
6. Fill in the `Power-Up Name` input with an identifiable name (Eg. `GitLab Test`)
7. Fill in the `Manifest URL` input with the https ngrok url from step 2 with the suffix `manifest.json` (Eg. ` https://fff56381.ngrok.io/manifest.json`)
8. Click `Save`
9. Click `Go to Your Boards` on the top right corner
10. Select a board under the team you designated in step 4
11. Click `Show Menu` on the top right (if the menu is not already opened)
12. Click on `Power-Ups`
13. Scroll down the list and click `Enable` next to the Power-Up named `GitLab`

## Releasing a new version

1. Merge `master` into `production` using a MR (Do not squash commits)
2. After merging, create a new tag with the correct version number

GitLab pages will auto-deploy the changes and it will auto-reflect on Trello's side.
If the new version updates the UI, please reach out to Trello team and have them update their screenshots.

## Features
- Attach merge requests to Trello cards

## Helpful Links

[Trello Power-Up Documentation](https://developers.trello.com/power-ups)

## Contributing
Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.
