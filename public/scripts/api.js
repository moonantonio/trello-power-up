/* global axios:false */

function getAllProjects(auth) {
  return axios.get(auth.api + '/projects', {
    params: {
      membership: true,
      private_token: auth.token,
      with_merge_requests_enabled: true,
      simple: true,
      per_page: 100,
      order_by: 'last_activity_at'
    }
  });
}

function getAllMergeRequests(auth, projectId) {
  return axios.get(auth.api + '/projects/' + projectId + '/merge_requests', {
    params: {
      private_token: auth.token,
      view: 'simple',
      per_page: 100
    }
  });
}

function isValidCredentials(endpoint, token) {
  return axios.get(endpoint + '/version', {
    params: {
      private_token: token
    }
  })
  .then(function validCredentials() {
    return true;
  })
  .catch(function invalidCredentials() {
    return false;
  });
}

window.api = {
  getAllProjects: getAllProjects,
  getAllMergeRequests: getAllMergeRequests,
  isValidCredentials: isValidCredentials
};
